#include <ansi_c.h>
#include <iso646.h>

#include <userint.h>
#include <utility.h>
#include <toolbox.h>

#include "ElementBitmap.h"

const char* ElementAbbrs[]={	// 10 per line. No [0]
	  "",  "H", "He", "Li", "Be",  "B",  "C",  "N",  "O",  "F",
	"Ne", "Na", "Mg", "Al", "Si",  "P",  "S", "Cl", "Ar",  "K",
	"Ca", "Sc", "Ti",  "V", "Cr", "Mn", "Fe", "Co", "Ni", "Cu",
	"Zn", "Ga", "Ge", "As", "Se", "Br", "Kr", "Rb", "Sr",  "Y",
	"Zr", "Nb", "Mo", "Tc", "Ru", "Rh", "Pd", "Ag", "Cd", "In",
	"Sn", "Sb", "Te",  "I", "Xe", "Cs", "Ba", "La", "Ce", "Pr",
	"Nd", "Pm", "Sm", "Eu", "Gd", "Tb", "Dy", "Ho", "Er", "Tm",
	"Yb", "Lu", "Hf", "Ta",  "W", "Re", "Os", "Ir", "Pt", "Au",
	"Hg", "Tl", "Pb", "Bi", "Po", "At", "Rn", "Fr", "Ra", "Ac",
	"Th", "Pa",  "U", "Np", "Pu", "Am", "Cm", "Bk", "Cf", "Es",
	"Fm", "Md", "No", "Lr", "Rf", "Db", "Sg", "Bh", "Hs", "Mt",
	"Ds", "Rg", "Cn", "Nh", "Fl", "Mc", "Lv", "Ts", "Og"
};

const char* ElementNames[]={	// 5 per line. No [0]
	"",            "Hydrogen",    "Helium",     "Lithium",    "Beryllium",
	"Boron",       "Carbon",      "Nitrogen",   "Oxygen",     "Fluorine",
	"Neon",        "Sodium",      "Magnesium",  "Aluminum",   "Silicon",
	"Phosphorus",  "Sulfur",      "Chlorine",   "Argon",      "Potassium",
	"Calcium",     "Scandium",    "Titanium",   "Vanadium",   "Chromium",
	"Manganese",   "Iron",        "Cobalt",     "Nickel",     "Copper",
	"Zinc",        "Gallium",     "Germanium",  "Arsenic",    "Selenium",
	"Bromine",     "Krypton",     "Rubidium",   "Strontium",  "Yttrium",
	"Zirconium",   "Niobium",     "Molybdenum", "Technetium", "Ruthenium",
	"Rhodium",     "Palladium",   "Silver",     "Cadmium",    "Indium",
	"Tin",         "Antimony",    "Tellurium",  "Iodine",     "Xenon",
	"Cesium",      "Barium",      "Lanthanum",  "Cerium",     "Praseodymium",
	"Neodymium",   "Promethium",  "Samarium",   "Europium",   "Gadolinium",
	"Terbium",     "Dysprosium",  "Holmium",    "Erbium",     "Thulium",
	"Ytterbium",   "Lutetium",    "Hafnium",    "Tantalum",   "Tungsten",
	"Rhenium",     "Osmium",      "Iridium",    "Platinum",   "Gold",
	"Mercury",     "Thallium",    "Lead",       "Bismuth",    "Polonium",
	"Astatine",    "Radon",       "Francium",   "Radium",     "Actinium",
	"Thorium",     "Protactinium","Uranium",    "Neptunium",  "Plutonium",
	"Americium",   "Curium",      "Berkelium",  "Californium","Einsteinium",
	"Fermium",     "Mendelevium", "Nobelium",   "Lawerencium","Rutherforium",
	"Dubnium",     "Seaborgium",  "Bohrium",    "Hassium",    "Meitnerium",
	"Darmstadtium","Roentgenium", "Copernicium","Nihonium",   "Flerovium",
	"Moscovium",   "Livermorium", "Tennessine", "Oganesson"
};

///////////////////////////////////////////////////////////////////////////////
/// HIFN	This call creates a bitmap of an atomic element or ion such as
/// HIFN	https://en.wikipedia.org/wiki/Chemical_symbol#/media/File:Atomic_Symbol_Mg.svg
/// HIFN	Exemple of use (see /DTEST_EB and main()):
/// HIFN		int Bt=ElementCreateBitmap(20 /*Ca*/, 1, 42, +2, VAL_YELLOW, VAL_GRAY /*TRANSPARENT*/);
/// HIFN		int Plot=PlotBitmap (Pnl, PNL_GRAPH, X, Y, 0, 0, NULL);
/// HIFN		SetCtrlBitmap(Pnl, PNL_GRAFRAW, Plot, Bt);
/// HIFN		DiscardBitmap(Bt);
/// HIFN	Yes, you have to use PlotBitmap(...NULL) followed by SetCtrlBitmap()
/// HIFN	The subs and sups are optional
/// HIPAR	AtomicNumber / 1..118. Pass 0 to discard the temporary panel
/// HIPAR	ShowAtomicNumber / If 1, shown on lower left
/// HIPAR	MassNumber / If 0, not shown, otherwise shown on upper left
/// HIPAR	Charge / If 0, not shown, otherwise shown on upper right
/// HIRET	Bitmap handle, you must release it with DiscardBitmap(). Or 0 if error.
///////////////////////////////////////////////////////////////////////////////
int ElementCreateBitmap(int AtomicNumber, int ShowAtomicNumber,
						 int MassNumber, int Charge,
						 int Color, int BgColor) {
	static int FirstCall=1;
	int H=40, W=70, tH, tW, Bitmap=0;
	int cH1=0, cW1=0, cH2=0, cW2=0, cH3=0, cW3=0;
	int Left, Right, Top, Bottom, FirstPass=1;
	char Str[20];
	#define SCH 3	// Shrink the vertical space between superscript and subscript (in half pixels)

	static int Pnl=0, Ctrl=0;	// We keep them for later, it's faster

	if (AtomicNumber==0) { if (Pnl) DiscardPanel(Pnl);; return 0; }
	if (AtomicNumber<1 or AtomicNumber>118) return 0;

	if (Pnl ==0) Pnl =NewPanel(0, "", 0, 0, H, W);
	if (Ctrl==0) Ctrl=NewCtrl (Pnl, CTRL_CANVAS, "", 0, 0);
	CanvasStartBatchDraw (Pnl, Ctrl);

	SetCtrlAttribute(Pnl, Ctrl, ATTR_WIDTH,  W);
	SetCtrlAttribute(Pnl, Ctrl, ATTR_HEIGHT, H);
	SetCtrlAttribute(Pnl, Ctrl, ATTR_PICT_BGCOLOR,   BgColor);
	SetCtrlAttribute(Pnl, Ctrl, ATTR_PEN_COLOR,      Color);
	SetCtrlAttribute(Pnl, Ctrl, ATTR_PEN_FILL_COLOR, BgColor);

	// To enable anti-aliasing for transparent canvas controls, you must set the ATTR_DRAW_POLICY attribute to VAL_DIRECT_TO_SCREEN
//	SetCtrlAttribute(Pnl, Ctrl, ATTR_DRAW_POLICY, VAL_DIRECT_TO_SCREEN);
	// LabWindows/CVI ignores this attribute for text you add using the PlotText, CanvasDrawText, and CanvasDrawTextAtPoint functions.
//	SetCtrlAttribute(Pnl, Ctrl, ATTR_ENABLE_ANTI_ALIASING, 1);
	// In other words, anti-aliasing doesn't work

Pass2:
	CanvasDrawTextAtPoint (Pnl, Ctrl, ElementAbbrs[AtomicNumber],
						   VAL_APP_META_FONT, MakePoint(W/2, H/2), VAL_CENTER_LEFT);
	GetTextDisplaySize (ElementAbbrs[AtomicNumber], VAL_APP_META_FONT, &tH, &tW);
	Right=W/2 +tW;
	Bottom=(H-SCH)/2+tH/2;

	if (FirstCall)	// Size ratio is 3/5
		CreateMetaFontWithCharacterSet ("SubSupMetaFont", VAL_APP_META_FONT,
									   tH*3/5, FirstCall=0, 0, 0, 0, 0, VAL_NATIVE_CHARSET);

	if (Charge) {	// Upper right
		sprintf(Str, "%d%c", abs(Charge), Charge>=0?'+':'-');
		CanvasDrawTextAtPoint(Pnl, Ctrl, Str, "SubSupMetaFont",
						MakePoint(W/2+tW, (H+SCH)/2), VAL_LOWER_LEFT);
		GetTextDisplaySize (Str, "SubSupMetaFont", &cH1, &cW1);
		Right+=cW1;	// Rightmost border
	}

	if (MassNumber) {	// Upper left
		sprintf(Str, "%d", MassNumber);
		CanvasDrawTextAtPoint(Pnl, Ctrl, Str, "SubSupMetaFont",
						MakePoint(W/2, (H+SCH)/2), VAL_LOWER_RIGHT);
		GetTextDisplaySize (Str, "SubSupMetaFont", &cH3, &cW3);
	}

	// Atomic number *after* mass number, otherwise the bcg color overlaps
	if (ShowAtomicNumber) {	// Lower left
		sprintf(Str, "%d", AtomicNumber);
		CanvasDrawTextAtPoint(Pnl, Ctrl, Str, "SubSupMetaFont",
						MakePoint(W/2, (H-SCH)/2), VAL_UPPER_RIGHT);
		GetTextDisplaySize (Str, "SubSupMetaFont", &cH2, &cW2);
		Bottom=(H-SCH)/2+cH2;
	}

	if (FirstPass) {
		// Compute the area of the effective printed text:
		if (Charge)          Top=H/2-cH1;
		else if (MassNumber) Top=H/2-cH3;
		else                 Top=H/2-tH/2;

		#define MAX(a,b) ((a)<(b) ? (b) : (a))
		if (MassNumber or ShowAtomicNumber) Left=W/2-MAX(cW2, cW3);
		else Left=W/2;

		// Unfortunately the following will scale the content.
		// We can't just clip the canvas by changinfg the size: We need to redraw everything
		SetCtrlAttribute(Pnl, Ctrl, ATTR_HEIGHT, Bottom-Top);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_WIDTH,  Right-Left);
		CanvasClear (Pnl, Ctrl, VAL_ENTIRE_OBJECT);
		W-=Left*2;
		H-=Top*2;
		FirstPass=0;
		goto Pass2;
	}

	CanvasEndBatchDraw (Pnl, Ctrl);
	int R=GetCtrlBitmap (Pnl, Ctrl, 0, &Bitmap);
//	DiscardPanel(Pnl);
	return R<0 ? 0 : Bitmap;	// Bitmaps can be negative, so find another way
}


///////////////////////////////////////////////////////////////////////////////
// The following 2 functions are entirely independant from the above
// They are just lookup tables
///////////////////////////////////////////////////////////////////////////////

typedef struct sList {
	const char* Str;
	int idx;
} tList;

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Compare 2 strings in a prototype compatible with qsort (and maintain index position)
static int CompareStr(const void *element1, const void *element2) {
	tList *L1=(tList*)element1,
		  *L2=(tList*)element2;
	return strcmp(L1->Str, L2->Str);
}

static int CompareStri(const void *element1, const void *element2) {
	tList *L1=(tList*)element1,
		  *L2=(tList*)element2;
	return stricmp(L1->Str, L2->Str);
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Find the atomic number based on the element abbreviation. O(logN)
/// HIRET	1..100 or so. 0 in case of absence
/// HIPAR	Abbr/Case sensitive. Pass NULL to free local allocation
///////////////////////////////////////////////////////////////////////////////
int ElementFromAbbr(char* Abbr) {
	static tList* OrderedCopy=NULL;
	int Nb=sizeof(ElementAbbrs)/sizeof(char*);
	if (Abbr==NULL) {
		if (OrderedCopy) free(OrderedCopy);; OrderedCopy=NULL;
		return 0;
	}
	if (OrderedCopy==NULL) {
		OrderedCopy=calloc(Nb, sizeof(tList));
		for (int i=0; i<Nb; i++)
			OrderedCopy[
				OrderedCopy[i].idx=i
			].Str=ElementAbbrs[i];
		qsort (OrderedCopy, Nb, sizeof(tList), CompareStr);
	}

	tList Key={Abbr,0};
	tList *Found = bsearch (&Key, OrderedCopy, Nb, sizeof(tList), CompareStr);
	return Found==NULL ? 0 : Found->idx;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Find the atomic number based on the element name. O(logN)
/// HIRET	1..100 or so. 0 in case of absence
/// HIPAR	Name/Case insensitive. Pass NULL to clean free local allocation
///////////////////////////////////////////////////////////////////////////////
int ElementFromName(char* Name) {
	static tList* OrderedCopy=NULL;
	int Nb=sizeof(ElementNames)/sizeof(char*);
	if (Name==NULL) {
		if (OrderedCopy) free(OrderedCopy);; OrderedCopy=NULL;
		return 0;
	}
	if (OrderedCopy==NULL) {
		OrderedCopy=calloc(Nb, sizeof(tList));
		for (int i=0; i<Nb; i++)
			OrderedCopy[
				OrderedCopy[i].idx=i
			].Str=ElementNames[i];
		qsort (OrderedCopy, Nb, sizeof(tList), CompareStri);
	}

	tList Key={Name,0};
	tList *Found = bsearch (&Key, OrderedCopy, Nb, sizeof(tList), CompareStri);
	return Found==NULL ? 0 : Found->idx;
}



///////////////////////////////////////////////////////////////////////////////
// The following part is independant of the rest except for a call to ElementCreateBitmap()

static ListType ListElems=NULL;

typedef struct sElem {
	int Z, ShowZ, A, q, Bitmap;	// We don't care about the colors, but we could
} tElem;

/// HIFN	Compare two elements
static int CVICALLBACK CompareElems(void *item1, void *item2) {
	tElem *E1=(tElem*)item1, *E2=(tElem*)item2;
	return E1->Z    !=E2->Z     ? E1->Z    -E2->Z :
		   E1->ShowZ!=E2->ShowZ ? E1->ShowZ-E2->ShowZ :
		   E1->A    !=E2->A     ? E1->A    -E2->A :
								  E1->q    -E2->q;
}

/// HIFN	Discard a cached bitmap
static int CVICALLBACK ClearBitmap(size_t index, void *ptrToItem, void *callbackData) {
	tElem *Elem=ptrToItem;
	int Sbole=SetBreakOnLibraryErrors(0);
	if (Elem->Bitmap) DiscardBitmap (Elem->Bitmap);; Elem->Bitmap=0;
	SetBreakOnLibraryErrors(Sbole);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Same parameters as ElementCreateBitmap
/// HIFN	But if you call it with the same params, it will return the same bitmap
/// HIFN	and save time in the process.
/// HIFN	So, do not discard those bitmaps yourself, but at the end call with 0 to discard them all
/// HIPAR	AtomicNumber / Pass 0 to clear the internal cache
///////////////////////////////////////////////////////////////////////////////
int ElementCacheBitmap(int AtomicNumber, int ShowAtomicNumber,
								int MassNumber, int Charge,
								int Color, int BgColor) {
	if (AtomicNumber==0) {
		ListApplyToEachEx(ListElems, 1, ClearBitmap, 0);
		ListDispose (ListElems); ListElems=NULL;
		return ElementCreateBitmap(0,0,0,0,0,0);
	}

	if (ListElems==NULL) ListElems = ListCreate (sizeof(tElem));

	tElem Current={AtomicNumber, ShowAtomicNumber, MassNumber, Charge, 0};
//	int index = ListFindItem (ListElems, &Current, FRONT_OF_LIST, CompareElems);
	int index = ListBinSearch(ListElems, &Current, CompareElems);
	if (index==0) {
		Current.Bitmap=ElementCreateBitmap(AtomicNumber, ShowAtomicNumber, MassNumber, Charge, Color, BgColor);
//		ListInsertItem   (ListElems, &Current, END_OF_LIST);
		ListInsertInOrder(ListElems, &Current, CompareElems);
		return Current.Bitmap;
	} else {
		tElem* Ptr=ListGetPtrToItem(ListElems, index);
		return Ptr->Bitmap;
	}
}



///////////////////////////////////////////////////////////////////////////////
#ifdef TEST_EB
#include <cvirte.h>

int main (int argc, char *argv[]) {
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;    /* out of memory */

	// Test lookup tables
	int H =ElementFromAbbr("H");
	int Li=ElementFromAbbr("Li");
	int U =ElementFromAbbr("U");

	H =ElementFromName("Hydrogen");
	Li=ElementFromName("lithium");
	U =ElementFromName("URANIUM");


	// Test graphic part
	int Pnl  = NewPanel(0, "Element Bitmaps", 100, 100, 400, 600);
	int Ctrl = NewCtrl (Pnl, CTRL_GRAPH_LS, "", 0, 0);
	SetCtrlAttribute(Pnl, Ctrl, ATTR_WIDTH,  600);
	SetCtrlAttribute(Pnl, Ctrl, ATTR_HEIGHT, 400);

	int Bt, Plot;
	Bt=ElementCreateBitmap(20, 1, 42, +2, VAL_BLUE, VAL_GRAY);
	Plot=PlotBitmap(Pnl, Ctrl, 10, 10, 0, 0, NULL);
	SetCtrlBitmap      (Pnl, Ctrl, Plot, Bt);
	DiscardBitmap(Bt);

	Bt=ElementCreateBitmap(20, 0, 0, -2, VAL_BLUE, VAL_WHITE);
	Plot=PlotBitmap(Pnl, Ctrl, 20, 20, 0, 0, NULL);
	SetCtrlBitmap  (Pnl, Ctrl, Plot, Bt);
	DiscardBitmap(Bt);

	Bt=ElementCreateBitmap(20, 0, 46, 0, VAL_BLUE, VAL_GRAY);
	Plot=PlotBitmap(Pnl, Ctrl, 30, 30, 0, 0, NULL);
	SetCtrlBitmap  (Pnl, Ctrl, Plot, Bt);
	DiscardBitmap(Bt);

	Bt=ElementCreateBitmap(30, 0, 0, 0, VAL_YELLOW, VAL_TRANSPARENT);
	Plot=PlotBitmap(Pnl, Ctrl, 40, 40, 0, 0, NULL);
	SetCtrlBitmap  (Pnl, Ctrl, Plot, Bt);
	DiscardBitmap(Bt);

	Bt=ElementCreateBitmap(40, 1, 0, 0, VAL_YELLOW, VAL_BLACK);
	Plot=PlotBitmap(Pnl, Ctrl, 50, 50, 0, 0, NULL);
	SetCtrlBitmap  (Pnl, Ctrl, Plot, Bt);
	DiscardBitmap(Bt);

	// For the autoscale
	PlotPoint (Pnl, Ctrl,  0,  0, VAL_NO_POINT, VAL_RED);
	PlotPoint (Pnl, Ctrl, 60, 60, VAL_NO_POINT, VAL_RED);

	DisplayPanel(Pnl);
	RunUserInterface();

	return 0;
}

#endif
