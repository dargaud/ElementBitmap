# ElementBitmap

This is a LabWindows/CVI function that will create a chemical element Bitmap,
same as https://en.wikipedia.org/wiki/Chemical_symbol#/media/File:Atomic_Symbol_Mg.svg
With mass number, atomic number and charge being optional.

You can then use the bitmap in various other places, for instance in a graph.

Compile with /DTEST_EB to see an example
