#ifndef __ELEMENT_BITMAP_H
#define __ELEMENT_BITMAP_H

extern const char* ElementAbbrs[], *ElementNames[];	// No [0]
extern int ElementFromAbbr(char* Abbr);
extern int ElementFromName(char* Name);

// See .c file
extern int ElementCreateBitmap(int AtomicNumber, int ShowAtomicNumber,
								int MassNumber, int Charge,
								int Color, int BgColor);
// Same as above but uses an internal cache for repeat calls.
// Use either function (but they don't clean up the same)
extern int ElementCacheBitmap(int AtomicNumber, int ShowAtomicNumber,
								int MassNumber, int Charge,
								int Color, int BgColor);

/* Summary of cleanup:
For ElementCreateBitmap: call DiscardBitmap for each of the obtained bitmaps
Then call ElementCreateBitmap(0, 0, 0, 0, 0, 0) for an internal panel

For ElementCacheBitmap: ElementCacheBitmap(0, 0, 0, 0, 0, 0) to flush all the
allocated internal bitmaps and the internal panel

I'm pretty sure those functions are not thread-safe.
*/


#endif